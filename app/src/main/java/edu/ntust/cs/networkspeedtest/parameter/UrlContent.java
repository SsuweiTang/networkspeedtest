package edu.ntust.cs.networkspeedtest.parameter;

/**
 * Created by wei on 2016/10/19.
 */

public class UrlContent {
    public static final String FILE="http://140.118.109.156/MyHomePage/GraphTheory/Week9_1.pptx";
    public static final String UPLOAD_PORT="21230";
    public static final String UPLOAD_OBJECT="upload";
    public static final String APKFILE="/data/app/edu.ntust.cs.most.ada-2/base.apk";
    public static final String DOWNFILEPATH="/sdcard/Download/9.pptx";


}
