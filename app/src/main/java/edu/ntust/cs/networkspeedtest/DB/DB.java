package edu.ntust.cs.networkspeedtest.DB;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author Ssu-Wei,Tang
 */
public class DB {

    private Context mCtx = null;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public DB(Context ctx) {
        this.mCtx = ctx;
    }

    public DB open() throws SQLException {
        dbHelper = new DatabaseHelper(mCtx);
        db = dbHelper.getWritableDatabase();

        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void beginTransaction() {
        db.beginTransaction();
    }

    public void transactionSuccessful() {
        db.setTransactionSuccessful();
    }

    public void endTransaction() {
        db.endTransaction();
    }


    public static final String TABLE_NETWORK_TEST = "network_test";
    public static final String KEY_TYPE = "network_type";
    public static final String KEY_UPLOAD_SPEED = "upload_speed";
    public static final String KEY_DOWNLOAD_SPEED = "download_speed";
    public static final String KEY_TOTAL_TIME = "total_time";
    public static final String KEY_TEST_TIME = "test_time";


    public long insertNetworkTest(String testTime, String type, String uploadSpeed, String downloadSpeed, String totalTime) {
        ContentValues args = new ContentValues();
        args.put(KEY_TEST_TIME, testTime);
        args.put(KEY_TYPE, type);
        args.put(KEY_UPLOAD_SPEED, uploadSpeed);
        args.put(KEY_DOWNLOAD_SPEED, downloadSpeed);
        args.put(KEY_TOTAL_TIME, totalTime);
        return db.insert(TABLE_NETWORK_TEST, null, args);
    }


    public Cursor getNetworkTest() {
        return db.rawQuery("select * from network_test order by test_time desc", null);
    }

    public int deleteNetworkTest() {
        return db.delete(TABLE_NETWORK_TEST, null, null);
    }




    // CREATE table
    private static final String DATABASE_NAME = "network.db";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_NETWORK_TEST = "CREATE TABLE IF NOT EXISTS network_test("
            + "test_time varchar," + "network_type varchar,"+"upload_speed varchar,"+"download_speed varchar,"+"total_time varchar"+");";



    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_NETWORK_TEST);
//            System.out.println("onCreate DB");
            Log.v("SS onCreate DB", CREATE_NETWORK_TEST);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }
}
