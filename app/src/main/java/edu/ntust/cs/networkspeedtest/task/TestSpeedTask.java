package edu.ntust.cs.networkspeedtest.task;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.TrafficStats;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.os.Handler;

import com.facebook.network.connectionclass.ConnectionClassManager;
import com.facebook.network.connectionclass.ConnectionQuality;

import java.text.SimpleDateFormat;
import java.util.Date;


import edu.ntust.cs.networkspeedtest.activity.SpeedTestActivity;
import edu.ntust.cs.networkspeedtest.activity.TestResultActivity;
import edu.ntust.cs.networkspeedtest.domain.Speed;
import edu.ntust.cs.networkspeedtest.domain.SpeedResultItem;
import edu.ntust.cs.networkspeedtest.util.NetworkConnectHandler;

/**
 * @author Ssu-Wei,Tang
 */


public class TestSpeedTask extends AsyncTask<Void, Integer, String> {

    private long start;
    private long end;
    private long totalTime;
    private SimpleDateFormat sdf;
    private long cureentTime = 0;
    private String type;
    private String uploadSpeed="";
    private String downloadSpeed="";



    private Context mContext;


    public TestSpeedTask(Context context) {
        start = System.currentTimeMillis();
        cureentTime = System.currentTimeMillis();
        SpeedTestActivity.showProgressDialog();
        mContext = context;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    @Override
    protected String doInBackground(Void... arg0) {

        int s = NetworkConnectHandler.getNetWorkType(mContext);

        Log.v("SS s", "type"+s);

        if (s == 4) {
            SpeedTestActivity.sp.setNetworkType("WIFI");

            WifiManager mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
            if (wifiInfo != null) {
                Integer linkSpeed = wifiInfo.getLinkSpeed();
//                uploadSpeed = linkSpeed + wifiInfo.LINK_SPEED_UNITS;
//                downloadSpeed=linkSpeed + wifiInfo.LINK_SPEED_UNITS;
            }
        } else if (s == 3) {
            SpeedTestActivity.sp.setNetworkType( "3G");



        }
        else if (s == 0) {

            SpeedTestActivity.sp.setNetworkType("未連網路");
        }
        sdf = new SimpleDateFormat("yyyy MMM dd, HH:mm:ss");


        return null;
    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        end = System.currentTimeMillis();
        totalTime = (end - start);
        Log.v("SS date", sdf.format(new Date(cureentTime)));
//        Log.v("SS type", type);
        SpeedTestActivity.sp.setTotalTime(totalTime+"ms");
        SpeedTestActivity.sp.setTestTime(sdf.format(new Date(cureentTime)));


//        SpeedTestActivity.progressDialog.dismiss();

        insertTestResutToSqlite( SpeedTestActivity.sp.getTestTime(),  SpeedTestActivity.sp.getNetworkType(),  SpeedTestActivity.sp.getUploadSpeed(),  SpeedTestActivity.sp.getUploadSpeed(),  SpeedTestActivity.sp.getTotalTime());
        Intent intent = new Intent();
        intent.setClass(mContext, TestResultActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString("type", type);
//        bundle.putString("totalTime", totalTime + "s");
//        bundle.putString("currentTime", sdf.format(new Date(curentTime)));
//        bundle.putString("uploadSpeed", uploadSpeed);
//        bundle.putString("downloadSpeed",downloadSpeed);
//        intent.putExtras(bundle);
        mContext.startActivity(intent);
//        ((Activity) mContext).finish();



    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }



    private void insertTestResutToSqlite(String testTime, String type, String uploadSpeed, String downloadSpeed, String totalTime) {
        SpeedTestActivity.mDbHelper.beginTransaction();

        try {
            Log.v("SS insert DB", testTime + type);
            SpeedTestActivity.mDbHelper.insertNetworkTest(testTime, type, uploadSpeed, downloadSpeed, totalTime);

            SpeedTestActivity.mDbHelper.transactionSuccessful();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            SpeedTestActivity.mDbHelper.endTransaction();
        }
    }



    private void downloadSpeed() {

    }

    private Speed getSpeed() {
        Speed sp =new Speed();
        long BeforeTime = System.currentTimeMillis();
        long TotalTxBeforeTest = TrafficStats.getTotalTxBytes();
        long TotalRxBeforeTest = TrafficStats.getTotalRxBytes();

        long TotalTxAfterTest = TrafficStats.getTotalTxBytes();
        long TotalRxAfterTest = TrafficStats.getTotalRxBytes();
        long AfterTime = System.currentTimeMillis();

        double TimeDifference = AfterTime - BeforeTime;

        double rxDiff = TotalRxAfterTest - TotalRxBeforeTest;
        double txDiff = TotalTxAfterTest - TotalTxBeforeTest;

        if((rxDiff != 0) && (txDiff != 0)) {
            double rxBPS = (rxDiff / (TimeDifference/1000)); // total rx bytes per second.
            double txBPS = (txDiff / (TimeDifference/1000)); // total tx bytes per second.
            sp.setDownloadSpeed(String.valueOf(rxBPS) + "B/s. Total rx = " + rxDiff);
            sp.setUploadSpeed(String.valueOf(txBPS) + "B/s. Total tx = " + txDiff);

        }
        else {
            sp.setDownloadSpeed("No uploaded or downloaded bytes.");
        }
        return sp;

    }


}

