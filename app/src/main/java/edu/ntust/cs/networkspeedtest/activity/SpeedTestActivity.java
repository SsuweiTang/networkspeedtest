package edu.ntust.cs.networkspeedtest.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.ntust.cs.networkspeedtest.DB.DB;
import edu.ntust.cs.networkspeedtest.R;
import edu.ntust.cs.networkspeedtest.domain.Speed;
import edu.ntust.cs.networkspeedtest.parameter.UrlContent;
import edu.ntust.cs.networkspeedtest.task.DownFileTask;
import edu.ntust.cs.networkspeedtest.task.SpeedTestDownTask;
import edu.ntust.cs.networkspeedtest.task.SpeedTestTask;
import edu.ntust.cs.networkspeedtest.task.TestSpeedTask;


/**
 * @author Ssu-Wei,Tang
 */
public class SpeedTestActivity extends AppCompatActivity {
    private Button buttonTestNetworkSpeed;
    private Button buttonTestNetworkSpeed2;
    private Button buttonTestResult;
    private EditText editTextUploadIp;
    public static DB mDbHelper;
    public static ProgressDialog progressDialog;
    public static Context mContext;
    public static Speed sp;
    public static SpeedTestTask  spTask=null;
    public static SpeedTestDownTask  spdownTask=null;
    public static  String uploadServerIp="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_test);
        findView();
        mContext = this;
        mDbHelper = new DB(SpeedTestActivity.this);
        mDbHelper.open();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mContext = this;
        mDbHelper = new DB(SpeedTestActivity.this);
        mDbHelper.open();

    }



    private void findView() {
        buttonTestNetworkSpeed = (Button) findViewById(R.id.buttonTestNetworkSpeed);
        buttonTestNetworkSpeed.setOnClickListener(SpeedTestClickListener);
        buttonTestNetworkSpeed2 = (Button) findViewById(R.id.buttonTestNetworkSpeed2);
        buttonTestNetworkSpeed2.setOnClickListener(SpeedTestClickListener);
        buttonTestResult= (Button) findViewById(R.id.buttonTestNetworkResult);
        buttonTestResult.setOnClickListener(SpeedTestClickListener);
        editTextUploadIp=(EditText)findViewById(R.id.editTextUploadIp);

    }
    public static ProgressDialog showProgressDialog() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage(mContext.getResources().getString(R.string.dialogTest));
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    private Button.OnClickListener SpeedTestClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonTestNetworkSpeed:
                    sp=new Speed();
//                    new TestSpeedTask(SpeedTestActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    spTask= new SpeedTestTask(SpeedTestActivity.this);
//                    spTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    spdownTask=new SpeedTestDownTask(SpeedTestActivity.this);
                    spdownTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    break;
                case R.id.buttonTestNetworkResult:
                    Intent intent = new Intent();
                    intent.setClass(SpeedTestActivity.this, TestResultActivity.class);
                    SpeedTestActivity.this.startActivity(intent);
                    break;
                case R.id.buttonTestNetworkSpeed2:
                    sp=new Speed();
                    if(editTextUploadIp.getText()!=null&&!editTextUploadIp.getText().toString().equals(""))
                    {
                        sp.setUploadIp(editTextUploadIp.getText().toString());
                        new DownFileTask(SpeedTestActivity.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                    else
                    {
                        Toast.makeText(SpeedTestActivity.this, "請輸入上傳server IP", Toast.LENGTH_SHORT).show();
                    }

                    break;
            }
        }


    };

    @Override
    public void onDestroy() {

        super.onDestroy();
        mDbHelper.close();
    }
}
