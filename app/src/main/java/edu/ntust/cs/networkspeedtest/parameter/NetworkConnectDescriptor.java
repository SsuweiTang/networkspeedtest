package edu.ntust.cs.networkspeedtest.parameter;

/**
 * @author Ssu-Wei,Tang
 */

public class NetworkConnectDescriptor {
    public static final int NETWORKTYPE_INVALID = 0;
    public static final int NETWORKTYPE_2G = 2;
    public static final int NETWORKTYPE_3G = 3;
    public static final int NETWORKTYPE_WIFI = 4;
}
