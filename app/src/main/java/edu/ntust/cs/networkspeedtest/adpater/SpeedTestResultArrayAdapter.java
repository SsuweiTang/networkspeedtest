package edu.ntust.cs.networkspeedtest.adpater;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import edu.ntust.cs.networkspeedtest.R;
import edu.ntust.cs.networkspeedtest.domain.SpeedResultItem;

/**
 * @author Ssu-Wei,Tang
 */

public class SpeedTestResultArrayAdapter extends ArrayAdapter<SpeedResultItem> {

    private Context context;
    private int id;
    private List<SpeedResultItem> items;


    public SpeedTestResultArrayAdapter(Context context, int resource,
                                       List<SpeedResultItem> items) {
        super(context, resource, items);

        this.context = context;
        this.id = resource;
        this.items = items;



    }

    @Override
    public SpeedResultItem getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(id, null);
        }


        SpeedResultItem o = items.get(position);
        TextView textViewTestTime = (TextView) v.findViewById(R.id.textViewTestTime);
        TextView textViewType = (TextView) v.findViewById(R.id.textViewType);
        TextView textViewDownloadSpeed = (TextView) v.findViewById(R.id.textViewDownloadSpeed);
        TextView textViewUploadSpeed = (TextView) v.findViewById(R.id.textViewUploadSpeed);
        TextView textViewTotalTime = (TextView) v.findViewById(R.id.textViewTotalTime);

        if (o != null) {
            if (textViewTestTime != null) {
                textViewTestTime.setText(o.getTestTime());
            }
            if (textViewType != null) {
                textViewType.setText(o.getNetworkType());
            }
            if (textViewDownloadSpeed != null) {
                textViewDownloadSpeed.setText(o.getDownloadSpeed());
            }
            if (textViewUploadSpeed != null) {

                textViewUploadSpeed.setText(o.getUploadSpeed());
            }
            if (textViewTotalTime != null) {
                textViewTotalTime.setText(o.getTotalTime());
            }


        }
        return v;
    }


}
