package edu.ntust.cs.networkspeedtest.domain;

import java.io.Serializable;

/**
 * @author Ssu-Wei,Tang
 */

public class SpeedResultItem implements Serializable {
    private static final long serialVersionUID = -8829119527625686266L;
    String networkType;
    String testTime;
    String uploadSpeed;
    String downloadSpeed;
    String totalTime;

    public SpeedResultItem(String networkType, String testTime, String uploadSpeed, String downloadSpeed,
                           String totalTime) {
        this.networkType = networkType;
        this.testTime = testTime;
        this.uploadSpeed = uploadSpeed;
        this.downloadSpeed = downloadSpeed;
        this.totalTime = totalTime;

    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public void setDownloadSpeed(String downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public void setUploadSpeed(String uploadSpeed) {
        this.uploadSpeed = uploadSpeed;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getNetworkType() {
        return networkType;
    }

    public String getTestTime() {
        return testTime;
    }

    public String getUploadSpeed() {
        return uploadSpeed;
    }

    public String getDownloadSpeed() {
        return downloadSpeed;
    }

    public String getTotalTime() {
        return totalTime;
    }

}
