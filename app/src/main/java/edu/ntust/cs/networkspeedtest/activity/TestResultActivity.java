package edu.ntust.cs.networkspeedtest.activity;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import edu.ntust.cs.networkspeedtest.DB.DB;
import edu.ntust.cs.networkspeedtest.R;
import edu.ntust.cs.networkspeedtest.adpater.SpeedTestResultArrayAdapter;
import edu.ntust.cs.networkspeedtest.domain.SpeedResultItem;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Ssu-Wei,Tang
 */

public class TestResultActivity extends AppCompatActivity {

    private ListView listViewTestResult;
    private SpeedTestResultArrayAdapter speedTestResultArrayAdapter;
    private DB mDbHelper = new DB(TestResultActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result);
        mDbHelper.open();
        findViewById();
        fillData();

    }


    private void fillData() {
//        Bundle extras = getIntent().getExtras();
        List<SpeedResultItem> listSpeedResultItem = new ArrayList<SpeedResultItem>();
        try {
            Cursor cursorKey = mDbHelper.getNetworkTest();
            while (cursorKey.moveToNext()) {
                listSpeedResultItem.add(new SpeedResultItem(cursorKey.getString(1), cursorKey.getString(0), cursorKey.getString(2), cursorKey.getString(3), cursorKey.getString(4)));
            }
            cursorKey.close();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }


//        try {
//            Bundle resultBundle = TestResultActivity.this.getIntent().getExtras();
//            String type = resultBundle.getString("type");
//            String currentTime = resultBundle.getString("currentTime");
//            String totalTime = resultBundle.getString("totalTime");
//            String uploadSpeed = resultBundle.getString("uploadSpeed");
//            String downloadSpeed = resultBundle.getString("downloadSpeed");
//            listSpeedResultItem.add(new SpeedResultItem(type, currentTime, uploadSpeed, downloadSpeed, totalTime));
//        } catch (Exception e) {
//        }
        speedTestResultArrayAdapter = new SpeedTestResultArrayAdapter(TestResultActivity.this, R.layout.list_item_speedtestresult, listSpeedResultItem);
        listViewTestResult.setAdapter(speedTestResultArrayAdapter);


    }

    private void findViewById() {

        listViewTestResult = (ListView) findViewById(R.id.listViewTestResult);
        listViewTestResult.setOnItemClickListener(testResultItemClickListener);
        listViewTestResult.setDivider(null);

    }

    private OnItemClickListener testResultItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> a, View v, int position, long id) {
            SpeedResultItem speedResultItem = speedTestResultArrayAdapter
                    .getItem(position);

            Toast.makeText(TestResultActivity.this, speedResultItem.getNetworkType(), Toast.LENGTH_SHORT).show();


        }
    };

    @Override
    public void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }

}
