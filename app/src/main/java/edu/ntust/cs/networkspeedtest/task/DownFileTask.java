package edu.ntust.cs.networkspeedtest.task;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import edu.ntust.cs.networkspeedtest.activity.SpeedTestActivity;
import edu.ntust.cs.networkspeedtest.activity.TestResultActivity;
import edu.ntust.cs.networkspeedtest.parameter.UrlContent;
import edu.ntust.cs.networkspeedtest.util.NetworkConnectHandler;


/**
 * @author Ssu-Wei Tang
 */

public class DownFileTask extends AsyncTask<String, String, String> {
    private long start = 0;
    private long end = 0;
    private long downStart = 0;
    private long downEnd = 0;
    private long upStart = 0;
    private long upEnd = 0;
    private SimpleDateFormat sdf;
    private long cureentTime = 0;
    private Context mContext;
    private int upFilelengh = 0;
    private int downFilelengh = 0;
    private MultiValueMap<String, Object> map;
    private HttpHeaders headers;


    public DownFileTask(Context context) {
        SpeedTestActivity.showProgressDialog();
        mContext = context;
        start = 0;
        end = 0;
        downStart = 0;
        downEnd = 0;
        upStart = 0;
        upEnd = 0;
        cureentTime = 0;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        cureentTime = System.currentTimeMillis();
        start = System.currentTimeMillis();
        sdf = new SimpleDateFormat("yyyy MMM dd, HH:mm:ss");
        SpeedTestActivity.sp.setTestTime(sdf.format(new Date(cureentTime)));
        int s = NetworkConnectHandler.getNetWorkType(mContext);
        if (s == 4) {
            SpeedTestActivity.sp.setNetworkType("WIFI");
            WifiManager mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
            if (wifiInfo != null) {
                Integer linkSpeed = wifiInfo.getLinkSpeed();
//                uploadSpeed = linkSpeed + wifiInfo.LINK_SPEED_UNITS;
//                downloadSpeed=linkSpeed + wifiInfo.LINK_SPEED_UNITS;
            }
        } else if (s == 3) {
            SpeedTestActivity.sp.setNetworkType("3G");
        } else if (s == 0) {

            SpeedTestActivity.sp.setNetworkType("未連網路");
        }

        map = new LinkedMultiValueMap<String, Object>();
        map.add("file", new FileSystemResource(new File(
                UrlContent.APKFILE)));
        map.add("filename", start + ".apk");
        headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    }

    @Override
    protected String doInBackground(String... aurl) {
        ResponseEntity<String> response = null;

        try {
            URL url = new URL(UrlContent.FILE);
            URLConnection conection = url.openConnection();
            conection.connect();
            InputStream in = new BufferedInputStream(url.openStream(),
                    8192);
            FileOutputStream out = new FileOutputStream(new File(UrlContent.DOWNFILEPATH));
            byte[] b = new byte[1024];
            int count = 0;
            downStart = System.currentTimeMillis();
            while ((count = in.read(b)) != -1) {
                downFilelengh += count;
                out.write(b, 0, count);
            }
            out.flush();
            in.close();
            out.close();
            downEnd = System.currentTimeMillis();


            File file = new File(UrlContent.DOWNFILEPATH);
            if (file.exists()) {
                boolean deleted = file.delete();
                Log.d("SS", "deleted File: " + deleted);
            }

            upStart = System.currentTimeMillis();
            RestTemplate restTemplate = new RestTemplate(true);
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setConnectTimeout(9000000);
            restTemplate.setRequestFactory(requestFactory);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(
                    map, headers);

            try {
                response = restTemplate.exchange("http://" + SpeedTestActivity.sp.getUploadIp() + ":" +
                                UrlContent.UPLOAD_PORT + "/" + UrlContent.UPLOAD_OBJECT,
                        HttpMethod.POST, requestEntity, String.class);
                if (response.getStatusCode().equals(HttpStatus.OK)) {
                    Log.d("SS", "response: " + response.getBody());
                    upFilelengh = Integer.valueOf(response.getBody().replace("success:", ""));
                    return response.getBody();
                }
            } catch (HttpClientErrorException e) {
                Log.e("Network Error ", e.getLocalizedMessage(), e);

            }

        } catch (Exception e) {
        }
        return null;

    }

    protected void onProgressUpdate(String... progress) {

    }

    @Override
    protected void onPostExecute(String unused) {
        upEnd = System.currentTimeMillis();
        Log.d("SS", "upFilelengh: " + upFilelengh);
        Log.d("SS", "downFilelengh: " + downFilelengh);
        Log.d("SS", "donn S : " + (downEnd - downStart) / 1000);
        Log.d("SS", "up S : " + (upEnd - upStart) / 1000);

        end = System.currentTimeMillis();
        SpeedTestActivity.sp.setTotalTime(((end - start) / 1000) + "s");
        SpeedTestActivity.sp.setUploadSpeed(getSpeed(upEnd, upStart, upFilelengh));
        SpeedTestActivity.sp.setDownloadSpeed(getSpeed(downEnd, downStart, downFilelengh));
        insertTestResutToSqlite(SpeedTestActivity.sp.getTestTime(), SpeedTestActivity.sp.getNetworkType(), SpeedTestActivity.sp.getUploadSpeed(), SpeedTestActivity.sp.getDownloadSpeed(), SpeedTestActivity.sp.getTotalTime());
        Intent intent = new Intent();
        intent.setClass(mContext, TestResultActivity.class);
        mContext.startActivity(intent);
        SpeedTestActivity.progressDialog.dismiss();
    }

    private String getSpeed(long end, long start, int size) {
        DecimalFormat rateDf = new DecimalFormat("0.000");
        String speed = "";
        long ms = (end - start) ;
        double s=(double)ms/1000;
        s= (int)Math.round(s * 100.00) / 100.00;
        double rate = (((size / 1024) / (s)) * 8);

        Log.d("SS", "download ended: " + "ms "+ms +"sec "+s);
        Log.d("SS", "rate : " + rate);
        rate = Math.round(rate * 100.0) / 100.0;

        if (rate > 1000)
            speed = rateDf.format(rate / 1024).concat(" Mbps");
        else
            speed = rateDf.format(rate).concat(" Kbps");
        return speed;

    }


    private void insertTestResutToSqlite(String testTime, String type, String uploadSpeed, String downloadSpeed, String totalTime) {
        SpeedTestActivity.mDbHelper.beginTransaction();
        try {
            Log.d("SS insert DB", testTime + type);
            SpeedTestActivity.mDbHelper.insertNetworkTest(testTime, type, uploadSpeed, downloadSpeed, totalTime);

            SpeedTestActivity.mDbHelper.transactionSuccessful();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            SpeedTestActivity.mDbHelper.endTransaction();
        }
    }
}

