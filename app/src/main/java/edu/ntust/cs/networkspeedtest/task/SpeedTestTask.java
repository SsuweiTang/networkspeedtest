package edu.ntust.cs.networkspeedtest.task;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.math.BigDecimal;

import edu.ntust.cs.networkspeedtest.activity.SpeedTestActivity;
import fr.bmartel.speedtest.ISpeedTestListener;
import fr.bmartel.speedtest.SpeedTestError;
import fr.bmartel.speedtest.SpeedTestReport;
import fr.bmartel.speedtest.SpeedTestSocket;

/**
 * Created by wei on 2016/10/17.
 */

public class SpeedTestTask extends AsyncTask<Void, Void, String> {
    private Context mContext;
    public static long start;
    public SpeedTestTask(Context context) {
        mContext = context;
        SpeedTestActivity.showProgressDialog();
    }

    @Override
    protected String doInBackground(Void... params) {
        start = System.currentTimeMillis();
        SpeedTestSocket speedTestSocket = new SpeedTestSocket();


        speedTestSocket.addSpeedTestListener(new ISpeedTestListener() {

            @Override
            public void onDownloadFinished(SpeedTestReport report) {
                // called when download is finished
                BigDecimal bigRate = new BigDecimal(1000000);
                Log.v("speedtest", "[DL FINISHED] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[DL FINISHED] rate in bit/s   : " + (report.getTransferRateBit()).doubleValue() / 1000000);

                SpeedTestActivity.sp.setDownloadSpeed((report.getTransferRateBit()).doubleValue() / 1000000+"Mbps");
            }

            @Override
            public void onDownloadError(SpeedTestError speedTestError, String errorMessage) {
                // called when a download error occur
            }

            @Override
            public void onUploadFinished(SpeedTestReport report) {
                // called when an upload is finished
                BigDecimal bigRate = new BigDecimal(1000000);
                Log.v("speedtest", "[UL FINISHED] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[UL FINISHED] rate in bit/s   : " + (report.getTransferRateBit()).divide(bigRate, 2, BigDecimal.ROUND_HALF_UP));
                SpeedTestActivity.sp.setUploadSpeed((report.getTransferRateBit()).doubleValue() / 1000000+"Mbps");

            }

            @Override
            public void onUploadError(SpeedTestError speedTestError, String errorMessage) {
                // called when an upload error occur
            }

            @Override
            public void onDownloadProgress(float percent, SpeedTestReport report) {
                // called to notify download progress

                Log.v("speedtest", "[DL PROGRESS] progress : " + percent + "%");
                Log.v("speedtest", "[DL PROGRESS] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[DL PROGRESS] rate in bit/s   : " + report.getTransferRateBit());
            }

            @Override
            public void onUploadProgress(float percent, SpeedTestReport report) {
                // called to notify upload progress
                Log.v("speedtest", "[UL PROGRESS] progress : " + percent + "%");
                Log.v("speedtest", "[UL PROGRESS] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[UL PROGRESS] rate in bit/s   : " + report.getTransferRateBit());
            }

            @Override
            public void onInterruption() {
                // triggered when forceStopTask is called
            }
        });


//        speedTestSocket.startFtpDownload("speedtest.tele2.net", "/1MB.zip");

        speedTestSocket.startUpload("2.testdebit.info", "/", 1000000);

//        speedTestSocket.startDownload("2.testdebit.info", "/fichiers/1Mo.dat");
        return null;
    }
}
