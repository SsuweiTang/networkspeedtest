package edu.ntust.cs.networkspeedtest.task;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import edu.ntust.cs.networkspeedtest.activity.SpeedTestActivity;
import edu.ntust.cs.networkspeedtest.activity.TestResultActivity;
import edu.ntust.cs.networkspeedtest.util.NetworkConnectHandler;
import fr.bmartel.speedtest.ISpeedTestListener;
import fr.bmartel.speedtest.SpeedTestError;
import fr.bmartel.speedtest.SpeedTestReport;
import fr.bmartel.speedtest.SpeedTestSocket;

/**
 * @author Ssu-Wei,Tang
 */


public class SpeedTestDownTask extends AsyncTask<Void, Void, String> {
    private Context mContext;
    private long start;
    private long end;
    private long totalTime;
    private SimpleDateFormat sdf;
    private long cureentTime = 0;


    public SpeedTestDownTask(Context context) {
        mContext = context;
        SpeedTestActivity.showProgressDialog();
    }

    @Override
    protected String doInBackground(Void... params) {

        SpeedTestSocket speedTestSocket = new SpeedTestSocket();

        // add a listener to wait for speedtest completion and progress
        speedTestSocket.addSpeedTestListener(new ISpeedTestListener() {

            @Override
            public void onDownloadFinished(SpeedTestReport report) {
                // called when download is finished
                BigDecimal bigRate = new BigDecimal(1000000);
                Log.v("speedtest", "[DL FINISHED] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[DL FINISHED] rate in bit/s   : " + (report.getTransferRateBit()).doubleValue() / 1000000);


                start=SpeedTestActivity.spTask.start;
                end = System.currentTimeMillis();
                totalTime = (end - start)/1000;
                sdf = new SimpleDateFormat("yyyy MMM dd, HH:mm:ss");

                SpeedTestActivity.sp.setTotalTime(totalTime+"s");
                SpeedTestActivity.sp.setTestTime(sdf.format(new Date(SpeedTestActivity.spTask.start)));
                int s = NetworkConnectHandler.getNetWorkType(mContext);
                Log.v("SS s", "type"+s);
                Log.v("SS s", "upload"+(report.getTransferRateBit()).doubleValue() / 1000000+"Mbps");

                if (s == 4) {
                    SpeedTestActivity.sp.setNetworkType("WIFI");

                } else if (s == 3) {
                    SpeedTestActivity.sp.setNetworkType( "3G");
                }
                else if (s == 0) {

                    SpeedTestActivity.sp.setNetworkType("未連網路");
                }

                DecimalFormat rateDf = new DecimalFormat("0.000");
                SpeedTestActivity.sp.setDownloadSpeed(rateDf.format((report.getTransferRateBit()).doubleValue() / 1000000)+"Mbps");
//                SpeedTestActivity.sp.setDownloadSpeed("00");

                insertTestResutToSqlite( SpeedTestActivity.sp.getTestTime(),  SpeedTestActivity.sp.getNetworkType(),  SpeedTestActivity.sp.getUploadSpeed(),  SpeedTestActivity.sp.getDownloadSpeed(),  SpeedTestActivity.sp.getTotalTime());
                Intent intent = new Intent();
                intent.setClass(mContext, TestResultActivity.class);
                mContext.startActivity(intent);

                SpeedTestActivity.progressDialog.dismiss();


                SpeedTestActivity.sp.setDownloadSpeed((report.getTransferRateBit()).doubleValue() / 1000000+"Mbps");
            }

            @Override
            public void onDownloadError(SpeedTestError speedTestError, String errorMessage) {
                // called when a download error occur
            }

            @Override
            public void onUploadFinished(SpeedTestReport report) {
                // called when an upload is finished
                BigDecimal bigRate = new BigDecimal(1000000);
                Log.v("speedtest", "[UL FINISHED] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[UL FINISHED] rate in bit/s   : " + (report.getTransferRateBit()).divide(bigRate, 2, BigDecimal.ROUND_HALF_UP));




            }

            @Override
            public void onUploadError(SpeedTestError speedTestError, String errorMessage) {
                // called when an upload error occur
            }

            @Override
            public void onDownloadProgress(float percent, SpeedTestReport report) {
                // called to notify download progress

                Log.v("speedtest", "[DL PROGRESS] progress : " + percent + "%");
                Log.v("speedtest", "[DL PROGRESS] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[DL PROGRESS] rate in bit/s   : " + report.getTransferRateBit());
            }

            @Override
            public void onUploadProgress(float percent, SpeedTestReport report) {
                // called to notify upload progress
                Log.v("speedtest", "[UL PROGRESS] progress : " + percent + "%");
                Log.v("speedtest", "[UL PROGRESS] rate in octet/s : " + report.getTransferRateOctet());
                Log.v("speedtest", "[UL PROGRESS] rate in bit/s   : " + report.getTransferRateBit());
            }

            @Override
            public void onInterruption() {
                // triggered when forceStopTask is called
            }
        });


//        speedTestSocket.startFtpDownload("speedtest.tele2.net", "/1MB.zip");

        speedTestSocket.startUpload("2.testdebit.info", "/", 1000000);

//        speedTestSocket.startDownload("2.testdebit.info", "/fichiers/1Mo.dat");
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.v("speedtest", "[UL onPostExecute] END");


//        start=SpeedTestActivity.spTask.start;
//        end = System.currentTimeMillis();
//        totalTime = (end - start);
//        sdf = new SimpleDateFormat("yyyy MMM dd, HH:mm:ss");
//
//        SpeedTestActivity.sp.setTotalTime(totalTime+"ms");
//        SpeedTestActivity.sp.setTestTime(sdf.format(new Date(SpeedTestActivity.spTask.start)));
//
//
//        int s = NetworkConnectHandler.getNetWorkType(mContext);
//        Log.v("SS s", "type"+s);
//
//        if (s == 4) {
//            SpeedTestActivity.sp.setNetworkType("WIFI");
//
//            WifiManager mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
//            WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
//            if (wifiInfo != null) {
//                Integer linkSpeed = wifiInfo.getLinkSpeed();
////                uploadSpeed = linkSpeed + wifiInfo.LINK_SPEED_UNITS;
////                downloadSpeed=linkSpeed + wifiInfo.LINK_SPEED_UNITS;
//            }
//        } else if (s == 3) {
//            SpeedTestActivity.sp.setNetworkType( "3G");
//        }
//        else if (s == 0) {
//
//            SpeedTestActivity.sp.setNetworkType("未連網路");
//        }


//        insertTestResutToSqlite( SpeedTestActivity.sp.getTestTime(),  SpeedTestActivity.sp.getNetworkType(),  SpeedTestActivity.sp.getUploadSpeed(),  SpeedTestActivity.sp.getDownloadSpeed(),  SpeedTestActivity.sp.getTotalTime());
//        Intent intent = new Intent();
//        intent.setClass(mContext, TestResultActivity.class);
//        mContext.startActivity(intent);
//
//        SpeedTestActivity.progressDialog.dismiss();


    }

    private void insertTestResutToSqlite(String testTime, String type, String uploadSpeed, String downloadSpeed, String totalTime) {
        SpeedTestActivity.mDbHelper.beginTransaction();

        try {
            Log.v("SS insert DB", testTime + type+uploadSpeed);
            SpeedTestActivity.mDbHelper.insertNetworkTest(testTime, type, uploadSpeed, downloadSpeed, totalTime);

            SpeedTestActivity.mDbHelper.transactionSuccessful();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            SpeedTestActivity.mDbHelper.endTransaction();
        }
    }

}
